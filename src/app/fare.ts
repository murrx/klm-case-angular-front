import { Airport } from "./airport-search-box/airport";

export class Fare {
    origin: Airport;
    destination: Airport;
    price: Price;
}

class Price {
    amount: number;
    currency: string;
}