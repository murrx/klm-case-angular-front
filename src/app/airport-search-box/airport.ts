export class Airport {
    code: string;
    name: string;
    description: string
}