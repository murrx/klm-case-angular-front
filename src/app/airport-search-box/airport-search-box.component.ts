import { Component, OnInit, Input } from '@angular/core';
import { AirportService } from '../airport.service'
import { Airport } from './airport';
import { FormControl } from '@angular/forms';
import { FlightSelection } from '../flight-selection';

@Component({
  selector: 'app-airport-search-box',
  templateUrl: './airport-search-box.component.html',
  styleUrls: ['./airport-search-box.component.css']
})
export class AirportSearchBoxComponent implements OnInit{

  @Input() flightSelection: FlightSelection;
  airports: Airport[];
  airportForm = new FormControl('');

  constructor(private airportService: AirportService) { }

  ngOnInit(){
    this.onChanges();
  }

  onChanges(): void {
    this.airportForm.valueChanges.subscribe(val => {
      this.getAirportsByTerm(val);
    });
  }

  getAirports(): void {
    this.airportService.getAirports()
      .subscribe(airports => this.handleResults(airports));
  }

  getAirportsByTerm(term: string): void {
    this.airportService.getAriportsByTerm(term)
      .subscribe(airports => this.handleResults(airports));
  }

  getAriportsByKey(key: string): void {
    this.airportService.getAirportByKey(key)
      .subscribe(airport => this.handleResults([airport]));
  }

  handleResults(airports: Airport[]){
      this.airports = airports;
      if (this.isSingleAirportSelected()){
        this.selectCurrentAirport();
      }
  }

  private isSingleAirportSelected(): boolean{
    return this.airports.length === 1;
  }

  private selectCurrentAirport(): void {
        this.flightSelection.setAirport(this.airports[0])
  }
}

